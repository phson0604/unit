<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'abcxyz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'eF,^,?,lIpFdlrbH?%u}*#Z2LFmBTUa|a)Xr=i?VxLj UbU%Ou1@Rf$z48OJ&zv@' );
define( 'SECURE_AUTH_KEY',  'GIY>3$#7{FE+=u1UvWwuN;CDy%jlU: 6;dff9-ZO zO#9u33|2OXn-8-kxMr~.-[' );
define( 'LOGGED_IN_KEY',    'sUnjyCmJq>8s6f}Wy<|6w:TKN[7o}#Lsc!~FP39Qq*LC]ikLOooU>RFsf*hlRgUa' );
define( 'NONCE_KEY',        'BW(/Hww+~;6 MA?tZC7}| ~@P([sCRgz5z5x<DQ7u2rLYAcWpCgnPC04AG4bOz:W' );
define( 'AUTH_SALT',        'W7=pwUN{v*OSmtux|~# !^u9 *<OL:8e>3_&(g=a?p{%JTM^=,Y[A=H s;y1k&,h' );
define( 'SECURE_AUTH_SALT', ']-D*O+(8~&SNPiwfuS/frxTI56*=DEGs`G*9ZqW4%-7[=R+<IfKBQ?(33DS*8uba' );
define( 'LOGGED_IN_SALT',   '*]Z8@t?L}}4GQj2[z$u}{P);}6,bmy$OV&l#xN1=Q~sD>ac-R.y((cRD:)<+7alp' );
define( 'NONCE_SALT',       'qzx7%2lU+,xMjQL/mHb4x6F,_]c6wj`l{?10HB+ND9+^4W!PkZL&+)AlT,*I3CtI' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
